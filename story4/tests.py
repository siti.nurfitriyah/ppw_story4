from django.test import TestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.
class Lab7FunctionalTest(TestCase):

	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')        
		# chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)

	def tearDown(self):
	    self.selenium.quit()
	    super(Lab7FunctionalTest, self).tearDown()

	def test_name_is_blue(self):
		selenium = self.selenium
		# Opening the link we want to test
		selenium.get('http://pepew-fitri.herokuapp.com/')
		name = selenium.find_element_by_id('teksHOME').value_of_css_property('color')
		self.assertEqual(name, rgba(2, 52, 179, 1))

	def test_title_is_teks(self):
		selenium = self.selenium
		# Opening the link we want to test
		selenium.get('http://pepew-fitri.herokuapp.com/')
		font = selenium.find_element_by_id('teksHOME1').value_of_css_property('font-family')
		self.assertIn("Chau Philomene One", font)

	def test_name_location(self):
		selenium = self.selenium
		# Opening the link we want to test
		selenium.get('http://pepew-fitri.herokuapp.com/')
		name = selenium.find_element_by_id('teksHOME')
		self.assertEqual(name.location['x'], 0)
		self.assertEqual(name.location['y'], 402)

	def test_title_location(self):
		selenium = self.selenium
		# Opening the link we want to test
		selenium.get('http://pepew-fitri.herokuapp.com/')
		font = selenium.find_element_by_id('teksHOME1')
		print("loc subtitle:")
		print(font.location)
		self.assertEqual(font.location['x'], 0)
		self.assertEqual(font.location['y'], 508)

