from django.urls import path
'''from django.contrib import admin'''
from . import views
from django.conf.urls import url
from django.conf.urls.static import static

app_name = 'story4'


'''urlpatterns = [
	path('',views.home, name="home"),
	path('home',views.home, name="home"),
	path('about',views.aboutMe, name="aboutMe"),
	path('experience',views.experience, name="experience"),
	path('contact',views.contact, name="contact"),
	path('registration',views.registration, name="registration"),
	path('schedule',views.jadwal, name="jadwal"),
	path('forms',views.isi_form, name="isi_forms"),
	path('delete',views.deleteJadwal,name='deleteJadwal')
	]'''
	
urlpatterns = [
	path('',views.home, name="home"),
	path('home',views.home, name="home"),
	path('about',views.aboutMe, name="aboutMe"),
	path('experience',views.experience, name="experience"),
	path('contact',views.contact, name="contact"),
	path('registration',views.registration, name="registration"),
	path('forms',views.addform,name="forms"),
	path('schedules',views.objform,name='schedules'),
	path('delete',views.deletesched,name='delete')


	]