from django.db import models

# Create your models here.
class Entry(models.Model):
	activity = models.CharField(max_length=200)
	day = models.CharField(max_length=20)
	date = models.DateField()
	time = models.TimeField()
	place = models.CharField(max_length=100)
	category = models.CharField(max_length=100)

