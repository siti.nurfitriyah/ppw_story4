from django import forms
from .models import Entry

class mySchedule(forms.Form):

	activity = forms.CharField(label="Activitas", required=True)
	day = forms.CharField(label="Day")
	date = forms.DateField(widget=forms.DateInput(attrs={'type':'date'}), label="Date")
	time = forms.TimeField(widget=forms.TimeInput(attrs={'type':'time'}), label="Time")
	place = forms.CharField(label="Place") 
	category = forms.CharField(label="Category")

